# poc-protractor-portal-rh

Projeto com objetivo de automação E2E em javascript.

## Requerimentos

MINIMUM NODE VERSION:
```v12.14```

para instalar as dependencias: 
```yarn```

## executar o projeto:
### Chrome: 
```yarn start:chrome:qa```

### Chrome headless:
```yarn start:chrome:headless:qa```

### Chrome com projeto local
```start:chrome:qa:local```

### Validacao do codigo ESLint
```lint:fix```

## Report Cucumber
caminho: ```report/report/index.html```

## Execução dos testes em Headless
No arquivo config.js descomente o comando:
```'--headless'```

# Arquitetura

O projeto faz a criação da massa de testes montando o body utilizando dados random e realiza a requisição rest utilizando o supertest.

## Tecnologias

* Protractor
* Cucumber
* Supertest
* Chai
* Mssql

## Estrutura do projeto
```
└── poc-protractor-portal-rh
    ├── README.md
    ├── conf.js
    ├── configuration
    │   ├── chrome.conf.js
    │   └── chrome.headless.conf.js
    ├── features
    │   └── stepDefinitions
    ├── index.js
    ├── package.json
    ├── report
    │   └── teste.report
    ├── src
    │   ├── api
    │   │   ├── enums
    │   │   │   ├── billingType.enum.js
    │   │   │   ├── companyProduct.enum.js
    │   │   │   ├── contractType.enum.js
    │   │   │   ├── deliveryType.enum.js
    │   │   │   ├── exportGroupEnum.js
    │   │   │   ├── orderType.enum.js
    │   │   │   └── paymentType.enum.js
    │   │   ├── group
    │   │   │   └── generate
    │   │   │       └── grupoEmpresa.js
    │   │   └── user
    │   │       └── userApi.js
    │   ├── db
    │   │   ├── config
    │   │   │   └── qa
    │   │   │       └── controleAcesso.js
    │   │   └── user.sql.js
    │   ├── features
    │   │   ├── specs
    │   │   │   └── login_portal_rh.feature
    │   │   └── stepDefinitions
    │   │       └── login.steps.js
    │   ├── modules
    │   │   └── index.js
    │   ├── pages
    │   │   ├── base.page.js
    │   │   ├── login
    │   │   │   └── login.page.js
    │   │   └── order
    │   │       └── order.page.js
    │   ├── support
    │   │   └── hooks.js
    │   └── utils
    │       └── UIComponent.js
    └── yarn.lock
```