const sql = require('mssql');
const qaControleAcesso = require('./config/qa/controleAcesso.js');

module.exports = function SqlTest() {
  this.getUserBase = async value => {
    const pool = await sql.connect(qaControleAcesso);
    const response = await pool
      .request()
      .input('id_user', sql.Int, value)
      .query('select * from usuarios where id_usuario = @id_user');
    sql.close();
    return response.recordset[0];
  };

  this.updateChangePassword = async value => {
    const pool = await sql.connect(qaControleAcesso);
    const response = await pool
      .request()
      .input('id_user', sql.Int, value)
      .query(
        "update usuarios set senha = PWDENCRYPT('Teste@123') where id_usuario = @id_user",
      );
    sql.close();
    console.log(response.rowsAffected);
  };
};
