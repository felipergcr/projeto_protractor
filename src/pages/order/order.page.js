const UIComponent = require('../../utils/UIComponent.js');

class OrderPage extends UIComponent {
  selector = $('#root');

  order = {
    fieldGroupName: this.selector.$('#selected_group_name'),
  };

  waitLoadingGroupName = async text => {
    await this.waitElementVisibleText(this.order.fieldGroupName, text);
  };

  waitGroupNameVisible = async () => {
    await this.waitVisibleElement(this.order.fieldGroupName);
  };

  getTextGroupName = async () => {
    await this.order.fieldGroupName.getText();
  };
}

module.exports = OrderPage;
