module.exports = {
  async go(site) {
    await browser.get(site);
  },

  async getTitle() {
    return browser.getTitle();
  },
};
