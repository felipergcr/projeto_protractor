const UIComponent = require('../../utils/UIComponent.js');
const CompanyGroup = require('../../api/user/userApi.js');
const DbQA = require('../../db/user.sql.js');

const DB = new DbQA();
const UserAPI = new CompanyGroup();

class LoginPage extends UIComponent {
  selector = $('#root');

  input = {
    user: this.selector.$('#cpf'),
    password: this.selector.$('#password'),
  };

  doLoginButton = this.selector.$('#lg_btn_submit');

  doLogin = async (usuario, senha) => {
    // this.waitUntilDisplayed();
    await this.waitVisibleElement(this.input.user);
    await this.input.user.sendKeys(usuario);
    await this.input.password.sendKeys(senha);
    await this.doLoginButton.click();
  };

  createGroup = async paramGroup => {
    const response = await UserAPI.postGroup(paramGroup);
    const idUser = await response.body.usuarios[0].idUsuario;
    await DB.updateChangePassword(idUser);
    return response.dataCreate;
  };
}
module.exports = LoginPage;
