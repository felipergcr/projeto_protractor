const { Given, When, Then, BeforeAll, setDefaultTimeout } = require('cucumber');

setDefaultTimeout(100 * 1000);

module.exports = {
  Given,
  When,
  Then,
  BeforeAll,
};
