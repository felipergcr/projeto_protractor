const chai = require('chai');
const { Given, When, Then } = require('../../modules/index.js');
const LoginPage = require('../../pages/login/login.page.js');
const OrderPage = require('../../pages/order/order.page.js');
const enumGroup = require('../../api/enums/exportGroupEnum.js');

const { expect } = chai;
const loginPage = new LoginPage();
const orderPage = new OrderPage();
let globalUserData;

Given(
  'Que eu possua grupo com os dados {string}, {string}, {string}, {string}, {string}',
  async (faturamento, pagamento, entrega, contrato, pedido) => {
    const paramGroup = {
      tipoFaturamento: faturamento,
      tipoPagamento: pagamento,
      tipoProduto: enumGroup.companyProduct.VR_VA,
      tipoEntrega: entrega,
      tipoContrato: contrato,
      tipoPedido: pedido,
    };
    globalUserData = await loginPage.createGroup(paramGroup);
  },
);

When('faco login no portal rh', async () => {
  await loginPage.doLogin(globalUserData.userData.cpf, 'Teste@123');
});

Then('valido a exibicao do grupo', async () => {
  await orderPage.waitGroupNameVisible();
  const groupNameVisibleFront = await orderPage.getTextGroupName();
  expect(groupNameVisibleFront).to.equal(globalUserData.groupName);
});
