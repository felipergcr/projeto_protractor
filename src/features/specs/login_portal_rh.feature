Feature: Validacao login

    Scenario Outline: Validacao de login com grupo gerado randomicamente 
        Given Que eu possua grupo com os dados <tipoFaturamento>, <tipoPagamento>, <tipoEntrega>, <tipoContrato>, <tipoPedido>
        When faco login no portal rh
        Then valido a exibicao do grupo

        Examples:
            | tipoFaturamento | tipoPagamento | tipoEntrega | tipoContrato | tipoPedido     |
            | "CENTRALIZADO"  | "BOLETO"      | "RH"        | "POS"        | "CENTRALIZADO" |