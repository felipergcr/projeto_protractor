class UIComponent {
  selector = undefined;

  constructor(selector = undefined) {
    this.selector = selector;
  }

  checkSelectorExist = () => {
    if (this.selector === undefined) {
      throw new TypeError('Deve ser definido um contrutor');
    }
  };

  isDisplayed = () => {
    this.checkSelectorExist();
    return ExpectedConditions.visibilityOf(this.selector)();
  };

  waitUntilDisplayedTimeout = 30000;

  waitVisibleElement = async element => {
    await browser.wait(
      ExpectedConditions.visibilityOf(element),
      this.waitUntilDisplayedTimeout,
    );
  };

  waitElementVisibleText = async (element, text) => {
    await browser.wait(
      ExpectedConditions.textToBePresentInElement(element, text),
      this.waitUntilDisplayedTimeout,
    );
  };

  waitUntilDisplayed = () => {
    this.checkSelectorExist();

    browser.wait(
      () => this.isDisplayed(),
      this.waitUntilDisplayedTimeout,
      `Failed while waiting for "${this.selector.locator()}" of Page Object Class '${
        this.constructor.name
      }' to display.`,
    );
  };
}

module.exports = UIComponent;
