const faker = require('faker');
const cnpjFaker = require('cnpj');
const cpfFaker = require('cpf');

class companyGroup {
  emailDefault = { default: 'samuel.severino.ext@conductor.com.br' };

  cnpjRandom = () => {
    cnpjFaker
      .generate()
      .replace('.', '')
      .replace('/', '')
      .replace('-', '')
      .replace('.', '');
  };

  groupName = {
    nomeGrupo: faker.name.findName(),
    nomeSubgrupo: faker.name.findName(),
  };

  parameters = param => {
    const {
      tipoPagamento,
      tipoFaturamento,
      tipoProduto,
      tipoEntrega,
      tipoContrato,
      tipoPedido,
    } = param;
    return {
      emailNotaFiscal: this.emailDefault.default,
      tipoFaturamento,
      tipoContrato,
      prazoPagamento: 90,
      limiteCreditoDisponivel: 2000.01,
      segmentoEmpresa: 'E1_DIGITAL',
      tipoPedido,
      modeloCartao: 'COM_NOME',
      modeloPedido: 'WEB',
      tipoEntrega,
      transferencia: 'HABILITADA',
      tipoPagamento,
      tipoProduto,
      dataVigenciaLimite: [2020, 12, 12],
    };
  };

  condicoes = {
    valorMinimoVR: 20.1,
    valorMinimoVA: 20.1,
    taxaAdministracao: 20.1,
    taxaEntrega: 20.1,
    taxaDisponibilizacaoCredito: 20.1,
    taxaEmissaoCartao: 20.1,
    taxaReemissaoCartao: 20.1,
  };

  endereco = {
    logradouro: `Rua ${faker.name.findName()}`,
    numero: 41,
    complemento: 'teste',
    cep: '02258200',
    bairro: faker.name.findName(),
    cidade: 'São Paulo',
    uf: 'SP',
    codigoIbge: '3550308',
  };

  contatos = [
    {
      nome: faker.name.findName(),
      cpf: cpfFaker.generate(false),
      dataNascimento: [1990, 1, 1],
      email: 'samuel.severino.ext@conductor.com.br',
      dddTel: 11,
      numTel: 953629457,
    },
  ];

  empresaMatriz = {
    descricao: faker.name.findName(),
    cnpj: this.cnpjRandom(),
    razaoSocial: faker.name.findName(),
    dataContrato: [2020, 1, 1],
    nomeExibicao: faker.name.findName(),
    nomeFantasia: faker.name.findName(),
    banco: '341',
    agencia: '0983',
    contaCorrente: '343456',
    dvContaCorrente: '09',
    flagMatriz: 1,
    telefone: {
      ddd: '11',
      telefone: '22349807',
      ramal: '4356',
    },
    endereco: this.endereco,
    contatos: this.contatos,
  };

  dataJoin = param => ({
    condicoes: this.condicoes,
    empresaMatriz: this.empresaMatriz,
    parametros: this.parameters(param),
  });

  generateDataGroup = async param => {
    const dataJson = await Object.assign(this.groupName, this.dataJoin(param));
    const createdGroup = {
      dataCreate: {
        groupName: dataJson.nomeGrupo,
        cnpjCompany: dataJson.empresaMatriz.cnpj,
        userData: dataJson.empresaMatriz.contatos[0],
      },
      dataJson,
    };
    return createdGroup;
  };
}

module.exports = companyGroup;
