const companyProduct = require('./companyProduct.enum');
const billing = require('./billingType.enum.js');
const contractType = require('./contractType.enum.js');
const paymentType = require('./paymentType.enum.js');
const deliveryType = require('./deliveryType.enum.js');
const orderType = require('./orderType.enum.js');

module.exports = {
  companyProduct,
  billing,
  contractType,
  paymentType,
  deliveryType,
  orderType,
};
