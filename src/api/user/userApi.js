const request = require('supertest');
const DataGroup = require('../group/generate/grupoEmpresa.js');

const dataGenerate = new DataGroup();

module.exports = function CompanyGroup() {
  this.getUser = async () => {
    request(process.env.API_URL)
      .get('/usuarios')
      .query({
        cpf: 71420286102,
        idGrupoEmpresa: 250799,
      })
      .expect(200)
      .then();
  };

  this.postGroup = async paramGroup => {
    const jsonData = await dataGenerate.generateDataGroup(paramGroup);
    console.log(`BODY POST: \n${JSON.stringify(jsonData.dataJson)}`);
    const response = await request(process.env.API_URL)
      .post('/grupos-empresas')
      .send(JSON.stringify(jsonData.dataJson))
      .set('Content-Type', 'application/json;charset=UTF-8')
      .expect(201)
      .then();
    console.log('RESPONSE: ');
    console.log(response.body);

    jsonData.body = {
      ...response.body,
    };
    return jsonData;
  };
};
