module.exports.config = require('../conf.js')({
  capabilities: {
    browserName: 'chrome',
    chromeOptions: {
      args: ['--headless'],
    },
  },
});
