require('dotenv').config();

module.exports = providedConfig => {
  const defaultConfig = {
    framework: 'custom',
    frameworkPath: require.resolve('protractor-cucumber-framework'),

    baseUrl: process.env.BASE_URL,

    directConnect: true,

    // getPageTimeout: 6000,
    // allScriptsTimeout: 10,

    maxSessions: 1,

    plugins: [
      {
        package: require.resolve(
          'protractor-multiple-cucumber-html-reporter-plugin',
        ),
        options: {
          automaticallyGenerateReport: true,
          removeExistingJsonReportFile: true,
          reportName: 'Protractor JS ',
          pageFooter: '<div><p>TEAM<p><div>',
          pageTitle: 'Test report',
          displayDuration: true,
          customData: {
            data: [
              { label: 'Project', value: 'Front End' },
              { label: 'Sprint', value: '30' },
              { label: 'Release-Front', value: '1.21.0' },
            ],
          },
        },
      },
    ],

    specs: ['../src/features/specs/*.feature'],
    cucumberOpts: {
      tags: true,
      format: 'json:./report/cukereport.json',
      require: [
        '../src/features/stepDefinitions/**/*.steps.js',
        '../src/support/*.js',
      ],
    },

    onPrepare: async () => {
      browser.driver
        .manage()
        .window()
        .maximize();
      await browser.waitForAngularEnabled(false);
      await browser.driver
        .manage()
        .timeouts()
        .implicitlyWait(30000);
    },
  };
  return { ...defaultConfig, ...providedConfig };
};
